-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 24, 2022 at 09:49 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.16

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `d9a8fele89gpo6aw`
--
CREATE DATABASE IF NOT EXISTS `d9a8fele89gpo6aw` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `d9a8fele89gpo6aw`;

-- --------------------------------------------------------

--
-- Table structure for table `books`
--
-- Creation: Apr 24, 2022 at 09:40 PM
--

DROP TABLE IF EXISTS `books`;
CREATE TABLE IF NOT EXISTS `books` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(100) NOT NULL,
  `AUTHOR` varchar(100) NOT NULL,
  `PUBLISHER` varchar(100) NOT NULL,
  `DATE` varchar(100) NOT NULL,
  `GENRE` varchar(100) NOT NULL,
  `ISBN` varchar(100) NOT NULL,
  `CHECKED_OUT` tinyint(1) DEFAULT '0',
  `CHECKOUT_USER_ID` int(11) DEFAULT NULL,
  `CHECKOUT_DATE` date DEFAULT NULL,
  `RETURN_DATE` date DEFAULT NULL,
  `DUE_DATE` date DEFAULT NULL COMMENT '\n',
  `IMG` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TITLE_UNIQ_IDX1` (`TITLE`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `books`
--

TRUNCATE TABLE `books`;
--
-- Dumping data for table `books`
--

INSERT INTO `books` (`ID`, `TITLE`, `AUTHOR`, `PUBLISHER`, `DATE`, `GENRE`, `ISBN`, `CHECKED_OUT`, `CHECKOUT_USER_ID`, `CHECKOUT_DATE`, `RETURN_DATE`, `DUE_DATE`, `IMG`) VALUES
(1, 'In Search of Lost Time', 'Marcel Proust', 'Modern Library', '2003-06-03', 'Classic Literature', '978-0812969641', 0, NULL, NULL, NULL, NULL, NULL),
(2, 'Ulysses', 'James Joyce', 'CreateSpace Independent Publishing Platform', '2019-01-24', 'Classic Literature', '978-1505297546', 0, NULL, NULL, NULL, NULL, NULL),
(3, 'Don Quixote', 'Miguel de Cervantes', 'Penguin Classics', '2003-02-25', 'Classic Literature', '978-0142437230', 0, NULL, NULL, '2022-04-24', NULL, 'quixote.jpg'),
(4, 'One Hundred Years of Solitude', 'Gabriel Garcia Marquez', 'Harper Perennial Modern Classics', '2006-02-21', 'Classic Literature', '0060883286', 0, NULL, NULL, NULL, NULL, NULL),
(5, 'The Great Gatsby', 'F. Scott Fitzgerald', 'Independently published', '2021-01-03', 'Classic Literature', '979-8581484234', 0, NULL, NULL, NULL, NULL, 'gatsby.jpeg'),
(6, 'Moby Dick', 'Herman Melville', 'Independently published', '2021-10-08', 'Classic Literature', '979-8492398590', 0, NULL, NULL, NULL, NULL, 'moby.jpg'),
(7, 'War and Peace', 'Leo Tolstoy', 'Vintage', '2008-12-02', 'Classic Literature', '978-1400079988', 0, NULL, NULL, NULL, NULL, NULL),
(8, 'Hamlet', 'William Shakespeare', 'Simon & Schuster', '1992-07-01', 'Classic Literature', '978-0743477123', 0, NULL, NULL, NULL, NULL, NULL),
(9, 'The Odyssey', 'Homer', 'Candlewick', '2010-10-12', 'Classic Literature', '978-0763642686', 0, NULL, NULL, NULL, NULL, NULL),
(10, 'Madame Bovary', 'Gustave Flaubert', 'Penguin Classics', '2011-10-04', 'Classic Literature', '978-0143106494', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--
-- Creation: Apr 24, 2022 at 09:40 PM
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `CARTID` int(11) NOT NULL AUTO_INCREMENT,
  `USERID` int(11) DEFAULT NULL,
  `BOOKID` int(11) DEFAULT NULL,
  `TITLE` varchar(100) DEFAULT NULL,
  `DATE` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CARTID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `cart`
--

TRUNCATE TABLE `cart`;
-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--
-- Creation: Apr 24, 2022 at 09:40 PM
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `failed_jobs`
--

TRUNCATE TABLE `failed_jobs`;
-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--
-- Creation: Apr 24, 2022 at 09:40 PM
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `migrations`
--

TRUNCATE TABLE `migrations`;
--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `milestone`
--
-- Creation: Apr 24, 2022 at 09:40 PM
--

DROP TABLE IF EXISTS `milestone`;
CREATE TABLE IF NOT EXISTS `milestone` (
  `idMilestone` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idMilestone`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `milestone`
--

TRUNCATE TABLE `milestone`;
--
-- Dumping data for table `milestone`
--

INSERT INTO `milestone` (`idMilestone`, `firstName`, `lastName`) VALUES
(1, 'test', 'test'),
(2, 'Kelly', 'Lamb'),
(3, 'Dustin', 'Johnson'),
(4, 'Kody', 'O\'Neill'),
(5, 'Mara', 'Munoz'),
(6, 'Vinson', 'Martin');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--
-- Creation: Apr 24, 2022 at 09:40 PM
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `password_resets`
--

TRUNCATE TABLE `password_resets`;
-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--
-- Creation: Apr 24, 2022 at 09:40 PM
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `personal_access_tokens`
--

TRUNCATE TABLE `personal_access_tokens`;
-- --------------------------------------------------------

--
-- Table structure for table `roles`
--
-- Creation: Apr 24, 2022 at 09:40 PM
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLENAME` varchar(45) NOT NULL,
  `DESCRIPTION` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `roles`
--

TRUNCATE TABLE `roles`;
--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`ID`, `ROLENAME`, `DESCRIPTION`) VALUES
(1, 'Normal User', 'A Customer'),
(2, 'Librarian', 'Librarian'),
(3, 'Admin', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Creation: Apr 24, 2022 at 09:40 PM
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRSTNAME` varchar(50) NOT NULL,
  `LASTNAME` varchar(50) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `MOBILE` varchar(100) NOT NULL,
  `PASSWORD` varchar(250) NOT NULL,
  `ROLE_ID` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EMAIL_UNIQ_IDX1` (`EMAIL`),
  KEY `USER_ROLE_ID_IDX` (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FIRSTNAME`, `LASTNAME`, `EMAIL`, `MOBILE`, `PASSWORD`, `ROLE_ID`) VALUES
(1, 'Librarian', 'Librarian', 'librarian@library.com', '(222) 222-2222', '22222222', 2),
(2, 'Admin', 'Admin', 'admin@libary.com', '(333) 333-3333', '33333333', 3),
(3, 'Test1', 'Test1', 'test1@test.com', '(111) 111-1111', '11111111', 1),
(4, 'test', 'test', 'test@test.com', '3135551212', 'testtest', 1),
(5, 'Kelly', 'Lamb', 'kl@kl.com', '1234567890', '12345678', 1),
(6, 'Kelly1', 'Lamb1', 'kl1@kl1.com', '1234567890', '11111111', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users1`
--
-- Creation: Apr 24, 2022 at 09:40 PM
--

DROP TABLE IF EXISTS `users1`;
CREATE TABLE IF NOT EXISTS `users1` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL DEFAULT ' ',
  `lastname` varchar(255) NOT NULL DEFAULT ' ',
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_mobile_unique` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users1`
--

TRUNCATE TABLE `users1`;
--
-- Dumping data for table `users1`
--

INSERT INTO `users1` (`id`, `firstname`, `lastname`, `email`, `mobile`, `email_verified_at`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'test', ' ', 'test@test.com', '3135551212', NULL, '$2y$10$tXQJ4YzVlH8.miI3L85xIuX2VDqZf3GyNHFo5P.UjO2lryhpyA/6q', NULL, NULL, '2022-04-24 13:00:19', '2022-04-24 13:00:19');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `ROLE_ID` FOREIGN KEY (`ROLE_ID`) REFERENCES `roles` (`ID`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
